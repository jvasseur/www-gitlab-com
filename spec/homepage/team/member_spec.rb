describe Gitlab::Homepage::Team::Member do
  describe '.all!' do
    subject { described_class.all! }

    it 'correctly integrates with data/team.yml' do
      expect(subject).not_to be_empty
      expect(subject).to all(be_a described_class)

      expect(subject.first.username).to eq 'dzaporozhets'
    end

    it 'correctly loads report titles' do
      expect(subject.first.reports_to_title).to match('CEO')
    end
  end

  describe '#assign' do
    let(:member) do
      described_class.new('username' => 'grzesiek',
                          'projects' => { 'gitlab-ce' => 'reviewer' })
    end

    let(:project) do
      Gitlab::Homepage::Team::Project
        .new('gitlab-ce', name: 'GitLab CE')
    end

    it 'creates a new project assignment' do
      member.assign(project)

      expect(member.assignments.count).to eq 1
      expect(member.assignments.first).to be_reviewer
    end
  end

  describe '#roles' do
    context 'when user has only one role in the project' do
      subject do
        described_class.new('projects' =>
                              { 'gitlab-ce' =>
                                'maintainer backend' })
      end

      it 'returns an inverted project role hash' do
        expect(subject.roles['gitlab-ce']).not_to be_empty
        expect(subject.roles['gitlab-ce']).to all(be_a String)
      end
    end

    context 'when user has only one role in the project' do
      subject do
        described_class.new('projects' =>
                              { 'gitlab-ce' =>
                                ['maintainer backend', 'owner'] })
      end

      it 'returns an inverted project role hash' do
        expect(subject.roles['gitlab-ce']).not_to be_nil
        expect(subject.roles['gitlab-ce']).to all(be_a String)
      end
    end
  end

  describe '#involved?' do
    subject do
      described_class.new('projects' =>
                            { 'gitlab-ce' =>
                              ['maintainer backend', 'owner'] })
    end

    context 'when user is involved in the project' do
      it 'indicates that user is involved in the project' do
        project = double(key: 'gitlab-ce')

        expect(subject.involved?(project)).to be true
      end
    end

    context 'when user is not involved in the project' do
      it 'indicates that user is not involved in the project' do
        project = double(key: 'gitlab-ee')

        expect(subject.involved?(project)).to be false
      end
    end
  end

  describe 'middleman compatibile delegated data sources' do
    subject { described_class.new('name' => 'grzesiek') }

    context 'when data key exists' do
      it 'delegates access to data source keys' do
        expect(subject.name).to eq 'grzesiek'
      end
    end

    context 'when data key does not exist' do
      it 'returns nil without raising an error' do
        expect(subject.country).to be_nil
      end
    end
  end
end
