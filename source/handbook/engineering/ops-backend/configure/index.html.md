---
layout: markdown_page
title: "Configure Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Configure Team

The Configure team is responsible for developing Ops focused features of GitLab
that relate to the "Configuration" and "Operations" stages of the DevOps
lifecycle. These refer to configuration of infrastructure as well as running
applications that are deployed via GitLab.

This team is currently building out more features for our Kubernetes integration
including the [Auto DevOps](/auto-devops) feature set and making it easier for
GitLab users to make the most of Kubernetes and DevOps best practices.

As per the [product categories](/handbook/product/categories/) this team
will also be responsible for building out new feature sets that will allow
GitLab users to easily make use of the following modern DevOps practices:

- ChatOps
- Serverless (or Functions As A Service)
- PaaS
- Runbook Configuration
- Chaos Engineering

For an understanding of where this team is going please take a look at [the
product vision](/direction/configure).
