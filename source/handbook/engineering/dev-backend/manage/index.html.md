---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Manage
{: #welcome}

The responsibilities of this team are described by the [Manage product
category](/handbook/product/categories/#manage). Among other things, this means 
working on GitLab's functionality around user, group and project administration, 
authentication, access control, and subscriptions.