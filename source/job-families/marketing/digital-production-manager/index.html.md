---
layout: job_page
title: "Digital Production Manager"
---

## Digital Production Manager

## Responsibilities 

- Develop multimedia (audio/video) content for brand and marketing initiatives.
- Manage production budget, vendors, schedules, and equipment. 
- Plan and manage production; write treatments, determine scope, scout and select locations and talent, coordinate shoots. 
- Manage creative, including scripting, storyboarding, and graphic elements, to ensure brand consistency. 
- Direct video shoots and manage production crew. 
- Collaborate closely with internal clients and stakeholders to assess and prioritize production needs.
- Manage video production for GitLab summit and user conference.
- Manage video library.
- Report on production metrics.

## Requirements 

- 5+ years of experience in video and audio production 
- Excellent communication, storytelling, and presentation of skills
- Strong organizational skills, ability to handle projects end-to-end
- Proficiency with video and audio software and equipment 
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission. 
